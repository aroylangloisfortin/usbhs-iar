/**
  *-----------------------------------------------------------------------------
  * @file    datalinkV2.c
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief
  *-----------------------------------------------------------------------------
  */

/* Includes ------------------------------------------------------------------*/
#include "global.h"

#define _DATALINKV2_GLOBAL
#include "datalinkV2.h"
#undef _DATALINKV2_GLOBAL


/* Local Defines -------------------------------------------------------------*/

/* Local Typedefs ------------------------------------------------------------*/
typedef enum
{
    sof,
    src,
    dst,
    code,
    count,
    allData,
    cs,
    eof
}dlV2RecvState_te;

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/

/* Local Prototype Functions -------------------------------------------------*/

/* Local Functions -----------------------------------------------------------*/

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Take a dtV2Message_ts and make a buffer to send over uart
  *
  * @param  message: The message to serialize. The data will be serialize in serialBuffer
  *         serialBuffer: The message will be serialize in this buffer. Make sure that it is initialize
  *         serialBufferSize: The size of serialBuffer. Must be MAX_DTV2_MESSAGE_SIZE
  *
  * @note   Make sure that serialBuffer have be initialize and have a size of DLV2_MAX_MESSAGE_SIZE
  *         because the receive data will be copy in this buffer.
  *
  * @retval index: The size of the return dataArray
  *                 0: serialBuffer not initiliaze or to small
  *                 others: the size of dataArray
  *
  *-----------------------------------------------------------------------------
  */
uint8_t dlV2Serializer(dlV2Message_ts* message, uint8_t* serialBuffer, uint8_t serialBufferSize)
{
    uint8_t index = 0;

    if(serialBuffer == NULL)
        index = 0;
    else if(serialBufferSize < DLV2_MAX_MESSAGE_SIZE)
        index = 0;
    else
    {
       serialBuffer[index++] = message->sof;
       serialBuffer[index++] = message->src;
       serialBuffer[index++] = message->dst;
       serialBuffer[index++] = message->code;
       serialBuffer[index++] = message->count;
       for(int i = 0; i < message->count; i++)
       {
           serialBuffer[index++] = message->data[i];
       }
       serialBuffer[index++] = message->cs;
       serialBuffer[index++] = message->eof;
    }

    return index;
}
/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Receive byte and set status to dataReady when a complete message is
  *         receive
  *
  * @param  recvMessage: remote or bypass message pointer
  *         recvByte: The byte to be analyze and add in recvMessage
  *         id: Remote or bypass id
  *
  * @note   None
  *
  * @retval The recvMessage.status = dataReady when a complete message is ready
  *
  *-----------------------------------------------------------------------------
  */
void dlV2RecvFSM(dlV2Message_ts* recvMessage, uint8_t recvByte, dlV2Id_te id)
{
   static dlV2RecvState_te remoteState = sof;
   static dlV2RecvState_te bypassState = sof;
   static dlV2RecvState_te pcState = sof;
   static uint8_t remoteRecvCount = 0;
   static uint8_t bypassRecvCount = 0;
   static uint8_t pcRecvCount = 0;

   uint8_t dataRecvCount = 0;
   dlV2RecvState_te currentState = sof;

   if(id == dlV2Bypass)
   {
      currentState = bypassState;
      dataRecvCount = bypassRecvCount;
   }
   else if(id == dlV2Remote)
   {
      currentState = remoteState;
      dataRecvCount = remoteRecvCount;
   }
   else if(id == dlV2Pc)
   {
      currentState = pcState;
      dataRecvCount = pcRecvCount;
   }

   switch (currentState)
   {
      case sof:
         recvMessage->sof = recvByte;
         if(recvMessage->sof == DLV2_SOF)
         {
            currentState = src;
         }
        else
        {
            currentState = sof;
        }
        recvMessage->status = unknow;
        break;

      case src:
         recvMessage->src = recvByte;
         currentState = dst;
         break;

      case dst:
         recvMessage->dst = recvByte;
         currentState = code;
         break;

      case code:
         recvMessage->code = recvByte;
         currentState = count;
         break;

      case count:
         recvMessage->count = recvByte;
         if(recvMessage->count == 0)
         {
            currentState = cs;
         }
         else
         {
            currentState = allData;
         }
         break;

      case allData:
         recvMessage->data[dataRecvCount] = recvByte;
         dataRecvCount++;
         if(dataRecvCount >= recvMessage->count)
         {
            dataRecvCount = 0;
            currentState = cs;
         }
         break;

      case cs:
         recvMessage->cs = dlV2Checksum(*recvMessage);
         if(recvByte != recvMessage->cs)
         {
            recvMessage->status = checksumError;
         }
         currentState = eof;
         break;

      case eof:
            recvMessage->eof = recvByte;
            if(recvMessage->eof == DLV2_EOF)
            {
                if(recvMessage->status == unknow)
                {
                    recvMessage->status = dataReady;
                }   
            }
            else 
            {
                if(recvMessage->status == unknow)
                {
                    recvMessage->status = eofError;
                }
                else
                {
                    recvMessage->status = csAndEofError;
                }
            }
            currentState = sof;
         break;

      default:
         break;
   }

   if(id == dlV2Bypass)
   {
      bypassState = currentState;
      bypassRecvCount = dataRecvCount;
   }
   else if( id == dlV2Remote)
   {
      remoteState = currentState;
      remoteRecvCount = dataRecvCount;
   }
   else if(id == dlV2Pc)
   {
      pcState = currentState;
      pcRecvCount = dataRecvCount;
   }
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Calculate the checksum for the Datalink V2 protocol
  *
  * @param  message: datalink V2 message structure
  *
  * @note   None
  *
  * @retval checksum: The checksum calculated on message
  *
  *-----------------------------------------------------------------------------
  */
uint8_t dlV2Checksum(dlV2Message_ts message)
{
    uint8_t checksum = 0;
    uint8_t i;

    checksum += message.code;
    checksum += message.count;
    checksum += message.dst;
    checksum += message.src;
    for(i = 0; i < message.count; i++)
    {
        checksum += message.data[i];
    }

    return checksum;
}



/* ---------------------------------------------------------------------------*/
