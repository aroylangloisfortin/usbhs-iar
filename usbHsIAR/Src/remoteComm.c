/**
  *-----------------------------------------------------------------------------
  * @file    remoteComm.c
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

/* Includes ------------------------------------------------------------------*/
#include "global.h"

#define _REMOTE_COMM_GLOBAL
#include "remoteComm.h"
#undef _REMOTE_COMM_GLOBAL

#include "gpioDriver.h"
#include "uartDriver.h"
#include "usbDriver.h"

/* Local Defines -------------------------------------------------------------*/

/* Local Typedefs ------------------------------------------------------------*/

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/

/* Local Prototype Functions -------------------------------------------------*/

/* Local Functions -----------------------------------------------------------*/

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the UART communication with Datalink
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void remoteCommInit(void)
{
   gpioUartRemoteInit();
   uartRemoteInit();
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Remote main task
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void remoteCommRun(void)
{
   static dlV2Message_ts message;
   static uint8_t sendBuffer[DLV2_MAX_MESSAGE_SIZE];
   static uint8_t messageSize;

   if(uartRemoteRecvFlag ==  true)
   {
      // Copy buffer
      uartCopyMessage(&message, uartRemote);
      uartRemoteRecvFlag = false;
      
      messageSize = dlV2Serializer(&message, sendBuffer, DLV2_MAX_MESSAGE_SIZE);
      usbSend(sendBuffer, messageSize);
   }

}

/* ---------------------------------------------------------------------------*/
