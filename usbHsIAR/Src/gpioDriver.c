/**
  *-----------------------------------------------------------------------------
  * @file    gpioDriver.c
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

/* Includes ------------------------------------------------------------------*/
#include "global.h"

#define _GPIO_DRIVER_GLOBAL
#include "gpioDriver.h"
#undef _GPIO_DRIVER_GLOBAL

#include "stm32f2xx_hal_gpio.h"

/* Local Defines -------------------------------------------------------------*/

/* Local Typedefs ------------------------------------------------------------*/

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/

/* Local Prototype Functions -------------------------------------------------*/

/* Local Functions -----------------------------------------------------------*/

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the GPIO clock
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void gpioInit(void)
{
   GPIO_InitTypeDef gpioInit;

   __HAL_RCC_GPIOH_CLK_ENABLE();
   __HAL_RCC_GPIOA_CLK_ENABLE();
   __HAL_RCC_GPIOB_CLK_ENABLE();
   __HAL_RCC_GPIOC_CLK_ENABLE();

   gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
   gpioInit.Pin = gpioLed1.pin | gpioLed2.pin | gpioLed3.pin;
   gpioInit.Pull = GPIO_PULLDOWN;
   gpioInit.Speed = GPIO_SPEED_MEDIUM;

   HAL_GPIO_Init(gpioLed1.port, &gpioInit);
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the GPIO for the Remote control UART
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void gpioUartRemoteInit(void)
{
   GPIO_InitTypeDef gpioInit;

   gpioInit.Mode = GPIO_MODE_AF_PP;
   gpioInit.Pull = GPIO_PULLUP;
   gpioInit.Speed = GPIO_SPEED_FREQ_HIGH;
   gpioInit.Alternate = GPIO_AF8_USART6;

   gpioInit.Pin = (gpioUartRemoteRx.pin | gpioUartRemoteTx.pin);
   HAL_GPIO_Init(gpioUartRemoteRx.port, &gpioInit);
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the GPIO for the Bypass UART
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void gpioUartBypassInit(void)
{
   GPIO_InitTypeDef gpioInit;

   gpioInit.Mode = GPIO_MODE_AF_PP;
   gpioInit.Pull = GPIO_PULLUP;
   gpioInit.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
   gpioInit.Alternate = GPIO_AF7_USART1;

   gpioInit.Pin = (gpioUartBypassRx.pin | gpioUartBypassTx.pin);
   HAL_GPIO_Init(gpioUartBypassRx.port, &gpioInit);
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the GPIO for the LED
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void gpioLedInit(void)
{
   GPIO_InitTypeDef gpioInit;

   gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
   gpioInit.Pull = GPIO_PULLDOWN;
   gpioInit.Speed = GPIO_SPEED_LOW;

   gpioInit.Pin = (gpioLed1.pin | gpioLed2.pin | gpioLed3.pin);
   HAL_GPIO_Init(gpioLed1.port, &gpioInit);
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Write a GPIO to low or high
  *
  * @param  gpio: GPIO structure that contain the port and pin #
  *         pinState: Set the GPIO as HIGH or LOW
  *
  * @note   ****Put in-line
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void gpioWritePin(gpio_ts gpio, bool pinState)
{
    HAL_GPIO_WritePin(gpio.port, gpio.pin, pinState);
}




/* ---------------------------------------------------------------------------*/
