/**
  *-----------------------------------------------------------------------------
  * @file    pcComm.c
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

/* Includes ------------------------------------------------------------------*/

#define _PC_COMM_GLOBAL
#include "pcComm.h"
#undef _PC_COMM_GLOBAL

#include "global.h"
#include "usbDriver.h"
#include "datalinkV2.h"
#include "gpioDriver.h"
#include "uartDriver.h"


/* Local Defines -------------------------------------------------------------*/
#define CUSTOM_BOARD_ID 0xFE
#define CUSTOM_BOARD_CODE_AUTO_CONNECT 0x01
#define CUSTOM_BOARD_CODE_SNIFFER 0x02
#define CUSTOM_BOARD_CODE_SIMULATION 0x03

#define MODE_SNIFFER 0
#define MODE_SIMULATION 1

/* Local Typedefs ------------------------------------------------------------*/

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/
bool modeSelection = MODE_SNIFFER;

/* Local Prototype Functions -------------------------------------------------*/

/* Local Functions -----------------------------------------------------------*/

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the usb communication with the PC
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void pcCommInit(void)
{
   usbInit();
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  PC USB main task
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void pcCommRun(void)
{
   static usbRecvStatus_te usbRecvStatus;
   static uint8_t usbRecvBuf[FIFO_WIDTH] = {0};
   static uint32_t usbRecvMessageLen = 0;
   static dlV2Message_ts pcRecvMessage = {.status = unknow};
   static uint8_t pcTransmitBuffer[DLV2_MAX_MESSAGE_SIZE];
   static uint8_t bufferSize;
   uint8_t i = 0;

   usbRecvStatus = usbReceive(usbRecvBuf, FIFO_WIDTH, &usbRecvMessageLen);

   // Data receive from USB?
   if(usbRecvStatus == dataReceive)
   {
      for(i = 0; i < usbRecvMessageLen; i++)
      {
         dlV2RecvFSM(&pcRecvMessage, usbRecvBuf[i], dlV2Pc);

         if(pcRecvMessage.status == dataReady)
         {
            // Simulation mode
            if(modeSelection == MODE_SIMULATION)
            {
                // Send data to remote starter
                if((pcRecvMessage.dst == DLV2_NODE_REMOTE_STARTER)) 
                {
                  bufferSize = dlV2Serializer(&pcRecvMessage, pcTransmitBuffer, DLV2_MAX_MESSAGE_SIZE);
                  
                  if(uartSendBytes(uartRemote, bufferSize, pcTransmitBuffer))
                  {
                      Error_Handler();
                  }
                  
                }
                //Send data to bypass
                else if((pcRecvMessage.dst == DLV2_NODE_DOOR_LOCK_INT) || (pcRecvMessage.dst == DLV2_NODE_TRANSPONDER_BYPASS))  
                {
                  bufferSize = dlV2Serializer(&pcRecvMessage, pcTransmitBuffer, DLV2_MAX_MESSAGE_SIZE);
                  uartSendBytes(uartBypass, bufferSize, pcTransmitBuffer);
                }
            }
            
            // Special command
            if(pcRecvMessage.dst == CUSTOM_BOARD_ID)
            {
                if(pcRecvMessage.code == CUSTOM_BOARD_CODE_AUTO_CONNECT)
                {
                    bufferSize = dlV2Serializer(&pcRecvMessage, pcTransmitBuffer, DLV2_MAX_MESSAGE_SIZE);
                    usbSend(pcTransmitBuffer, bufferSize);
                }
                else if(pcRecvMessage.code == CUSTOM_BOARD_CODE_SNIFFER)
                {
                  gpioWritePin(gpioLed2, HIGH);
                  gpioWritePin(gpioLed3, LOW);
                  modeSelection = MODE_SNIFFER;
                  uartEnableRxInterrupt(uartRemote);
                  uartEnableRxInterrupt(uartBypass);
                  
                }
                else if(pcRecvMessage.code == CUSTOM_BOARD_CODE_SIMULATION)
                {
                  gpioWritePin(gpioLed2, LOW);
                  gpioWritePin(gpioLed3, HIGH);
                  modeSelection = MODE_SIMULATION;
                  uartEnableRxInterrupt(uartRemote);
                  uartDisableRxInterrupt(uartBypass);
                }               
            }
         }

      }

   }
   else if(usbRecvStatus == bufferToSmall)
   {
      Error_Handler();
   }

}



/* ---------------------------------------------------------------------------*/
