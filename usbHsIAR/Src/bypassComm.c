/**
  *-----------------------------------------------------------------------------
  * @file    bypassComm.c
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

/* Includes ------------------------------------------------------------------*/
#include "global.h"

#define _BYPASS_COMM_GLOBAL
#include "bypassComm.h"
#undef _BYPASS_COMM_GLOBAL

#include "gpioDriver.h"
#include "uartDriver.h"
#include "datalinkV2.h"
#include "usbDriver.h"
#include "pcComm.h"

/* Local Defines -------------------------------------------------------------*/
#define CUSTOM_BOARD_CODE 0xFE

/* Local Typedefs ------------------------------------------------------------*/

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/

/* Local Prototype Functions -------------------------------------------------*/

/* Local Functions -----------------------------------------------------------*/

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the UART communication with Datalink
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void bypassCommInit(void)
{
   gpioUartBypassInit();
   uartBypassInit();
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Bypass main task. USART1 send/receive action
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void bypassCommRun(void)
{
   static dlV2Message_ts message;
   static uint8_t sendBuffer[DLV2_MAX_MESSAGE_SIZE];
   static uint8_t messageSize;
    
   if(uartBypassRecvFlag ==  true)
   {
      // Copy buffer
      uartCopyMessage(&message, uartBypass);
      uartBypassRecvFlag = false;
      
      // To remove (bypass simulator)
      message.src = DLV2_NODE_DOOR_LOCK_INT;
      message.cs += 4;
      
      messageSize = dlV2Serializer(&message, sendBuffer, DLV2_MAX_MESSAGE_SIZE);
      usbSend(sendBuffer, messageSize);
   }
   

}


/* ---------------------------------------------------------------------------*/
