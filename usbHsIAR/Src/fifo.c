/**
  *-----------------------------------------------------------------------------
  * @file    fifo.c
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

/* Includes ------------------------------------------------------------------*/
#include "global.h"

#define _FIFO_GLOBAL
#include "fifo.h"
#undef _FIFO_GLOBAL


/* Local Defines -------------------------------------------------------------*/

/* Local Typedefs ------------------------------------------------------------*/

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/

/* Local Prototype Functions -------------------------------------------------*/

/* Local Functions -----------------------------------------------------------*/

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Add data to the head
  *
  * @param  fifo  Specifies the fifo to work on.
  *         data  data to add in fifo
  *         dataSize  nombre of byte in data
  *
  * @note
  *
  * @retval fifo Status
  *
  *-----------------------------------------------------------------------------
  */
fifoStatus_te fifoAddData(fifo_ts *fifo, uint8_t *data, uint8_t dataSize)
{
  uint8_t i = 0;

  if(dataSize > FIFO_WIDTH)
  {
    return fifoWidthToSmall;
  }
  else if(fifo->head >= FIFO_SIZE)
  {
    return fifoSizeToSmall;
  }
  else
  {
    for(i = 0; i < dataSize; i++)
    {
      fifo->data[fifo->head][i] = data[i];
    }
    fifo->dataSize[fifo->head] = dataSize;
    fifo->head++;
  }
  
  return fifoOk;
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Remove data from the tail
  *
  * @param  fifo  Specifies the fifo to work on.
  *         data  fifo will copy data in this buffer
  *         dataSize  syze of the data buffer
  *         dataSizeCopied number of byte copied    
  *
  * @note
  *
  * @retval fifo Status
  *
  *-----------------------------------------------------------------------------
  */
fifoStatus_te fifoRemoveData(fifo_ts *fifo, uint8_t *data, uint8_t dataSize, uint8_t *dataSizeCopied)
{
  uint8_t i = 0;

  if(fifo->tail == fifo->head)
  {
    return fifoNoData;
  }
  else if(dataSize > FIFO_WIDTH)
  {
    return fifoWidthToSmall;
  }
  else
  {
    for(i = 0; i < dataSize; i++)
    {
       data[i] = fifo->data[fifo->tail][i];
    }
    *dataSizeCopied = fifo->dataSize[fifo->tail];
    fifo->tail++;

    if(fifo->tail == fifo->head)
    {
      fifo->tail = 0;
      fifo->head = 0;
    }
  }

  return fifoOk;
}


/* ---------------------------------------------------------------------------*/
