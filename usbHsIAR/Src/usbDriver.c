/* Includes ------------------------------------------------------------------*/

#define _USB_DRIVER_GLOBAL
#include "usbDriver.h"
#undef _USB_DRIVER_GLOBAL

#include "global.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"

#include "gpioDriver.h"

/* Local Defines -------------------------------------------------------------*/

/* Local Typedefs ------------------------------------------------------------*/

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/
USBD_HandleTypeDef hUsbDeviceFS;

/* Local Prototype Functions -------------------------------------------------*/

/* Local Functions -----------------------------------------------------------*/

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the usb communication with the PC
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void usbInit(void)
{
   USBD_Init(&hUsbDeviceHS, &HS_Desc, DEVICE_HS);

   USBD_RegisterClass(&hUsbDeviceHS, &USBD_CDC);

   USBD_CDC_RegisterInterface(&hUsbDeviceHS, &USBD_Interface_fops_HS);

   USBD_Start(&hUsbDeviceHS);
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the usb communication with the PC
  *
  * @param  buf: Data buffer
  *         len: Number of data to send
  *
  * @note   The data will be send directly in buf
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void usbSend(uint8_t* buf, uint16_t len)
{
   CDC_Transmit_HS(buf, len);
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Check if data is receive. If there is data ready, they will be copied
  *         in buf.
  *
  * @param  buf: Data buffer to copied the receive data
  *         bufLen: Data buffer size
  *         messageLen: The receive message length
  *
  * @note
  *
  * @retval dataReceive: Success ->Data Receive
  *         bufferToSmall: Fail -> buffer to small
  *         noDataReady: Success -> No Data
  *
  *-----------------------------------------------------------------------------
  */
usbRecvStatus_te usbReceive(uint8_t *buf, uint32_t bufLen, uint32_t *messageLen)
{
   uint32_t index = usbRxFifo.fifoPos;
   uint32_t i = 0;

   *messageLen = 0;

   if(index != 0)
   {
      if(usbRxFifo.fifoDataPos[index-1] <= bufLen)
      {
         for(i = 0; i < usbRxFifo.fifoDataPos[index-1]; i++)
         {
            buf[i] = usbRxFifo.fifoData[index-1][i];
         }
         *messageLen =  usbRxFifo.fifoDataPos[index-1];
         usbRxFifo.fifoPos--;
         return dataReceive;
      }
      usbRxFifo.fifoPos--;
      return bufferToSmall;
   }
   return noDataReady;
}





/* ---------------------------------------------------------------------------*/


