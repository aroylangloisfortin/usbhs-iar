/**
  *-----------------------------------------------------------------------------
  * @file    uartDriver.c
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

/* Includes ------------------------------------------------------------------*/
#include "global.h"


#define _UART_DRIVER_GLOBAL
#include "uartDriver.h"
#undef _UART_DRIVER_GLOBAL

#include "stm32f2xx_hal_usart.h"
#include "fifo.h"
#include "gpioDriver.h"


/* Local Defines -------------------------------------------------------------*/
#define REMOTE_BAUDRATE 9600
#define BYPASS_BAUDRATE 9600

/* Local Typedefs ------------------------------------------------------------*/
typedef enum
{
   uartTxReady,
   uartTxBusy,
}uartTxStatus_te;

typedef struct
{
   uartTxStatus_te   uartTxStatus;
   uint16_t          uartTxBytesSent;
   uint16_t          uartTxBytesToSend;
   uint8_t           uartTxBuffer[DLV2_MAX_MESSAGE_SIZE];
   fifo_ts           uartTxFifo;
}uartTx_ts;

/* Local Constants -----------------------------------------------------------*/

/* Local Variables -----------------------------------------------------------*/
static UART_HandleTypeDef uartRemoteHandle;
static UART_HandleTypeDef uartBypassHandle;

static dlV2Message_ts bypassRecvMessage = {.status = unknow};
static dlV2Message_ts remoteRecvMessage = {.status = unknow};

static uartTx_ts remoteTx = { .uartTxStatus = uartTxReady,
                              .uartTxBytesSent = 0,
                              .uartTxBytesToSend = 0,
                              .uartTxFifo.tail = 0,
                              .uartTxFifo.head = 0
                            };

/* Local Prototype Functions -------------------------------------------------*/


/* Local Functions -----------------------------------------------------------*/
/**
  *-----------------------------------------------------------------------------
  *
  * @brief  This function is the interrupt routine for the Bypass.
  *         This routine detect if it is a RX or TX interrupt and proceed to
  *         a collect the data in case of a RX interrupt or sends data over TX
  *         if it's a TX operation.
  *
  * @param  None
  *
  * @note   Also change function name in startup_stm32.s
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void USART1_IRQHandler(void)
{
   uint32_t status = USART1->SR;
   uint8_t byte = 0;

   // Data ready to read?
   if((status & UART_FLAG_RXNE) && (uartBypassRecvFlag == false) && (status ^ UART_FLAG_PE)
         && (status ^ UART_FLAG_FE) && (status ^ UART_FLAG_NE) && (status ^ UART_FLAG_ORE))
   {
      byte = (uint8_t)(USART1->DR & 0x00FF);
      dlV2RecvFSM(&bypassRecvMessage, byte, dlV2Bypass);

      if(bypassRecvMessage.status == dataReady)
      {
         uartBypassRecvFlag = true;
      }

   }
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  This function is the interrupt routine for the Remote.
  *         This routine detect if it is a RX or TX interrupt and proceed to
  *         a collect the data in case of a RX interrupt or sends data over TX
  *         if it's a TX operation.
  *
  * @param  None
  *
  * @note   Also change function name in startup_stm32.s
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void USART6_IRQHandler(void)
{
   uint32_t status = USART6->SR;
   uint8_t byte = 0;

   // Data to send ?
   if( (status & UART_FLAG_TXE) )
   {
      if(remoteTx.uartTxStatus == uartTxBusy)
      {
         /* If the TX operation is done the uart Tx is ready to send once again */
         if(remoteTx.uartTxBytesSent == remoteTx.uartTxBytesToSend)
         {
           // Data ready in the fifo?
           if(remoteTx.uartTxFifo.tail != remoteTx.uartTxFifo.head)
           {
              // Retrieve data 
              fifoRemoveData(&(remoteTx.uartTxFifo), remoteTx.uartTxBuffer, DLV2_MAX_MESSAGE_SIZE, &byte);
              remoteTx.uartTxBytesSent = 0;
              remoteTx.uartTxBytesToSend = byte;
             
              // Send data
              USART6->DR = remoteTx.uartTxBuffer[remoteTx.uartTxBytesSent];
              remoteTx.uartTxBytesSent++;
           }
           else
           {
              remoteTx.uartTxStatus = uartTxReady;
              __HAL_UART_DISABLE_IT(&uartRemoteHandle, UART_IT_TXE);
           }
         }
         else  /* Send the next byte over UART and set the ptr to the next byte*/
         {
            USART6->DR = remoteTx.uartTxBuffer[remoteTx.uartTxBytesSent];
            remoteTx.uartTxBytesSent++;
         }
      }
   }

   // Data to read?
   if((status & UART_FLAG_RXNE) && (uartRemoteRecvFlag == false) && (status ^ UART_FLAG_PE)
         && (status ^ UART_FLAG_FE) && (status ^ UART_FLAG_NE) && (status ^ UART_FLAG_ORE))
   {
      byte = (uint8_t)(USART6->DR & 0x00FF);
      dlV2RecvFSM(&remoteRecvMessage, byte, dlV2Remote);

      if(remoteRecvMessage.status == dataReady)
      {
         uartRemoteRecvFlag = true;
      }

   }
}

/* Global Functions ----------------------------------------------------------*/

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the UART peripheral to communicate with the remote control
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void uartRemoteInit(void)
{
   /* Initialize the peripheral clock */
   __HAL_RCC_USART6_CLK_ENABLE();

   /* Configure the UART parameters */
   uartRemoteHandle.Init.BaudRate = REMOTE_BAUDRATE;
   uartRemoteHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
   uartRemoteHandle.Init.Mode = UART_MODE_TX_RX;
   uartRemoteHandle.Init.OverSampling = UART_OVERSAMPLING_16;
   uartRemoteHandle.Init.Parity = UART_PARITY_NONE;
   uartRemoteHandle.Init.StopBits = UART_STOPBITS_1;
   uartRemoteHandle.Init.WordLength = UART_WORDLENGTH_8B;
   uartRemoteHandle.Instance = USART6;
   if(HAL_UART_Init(&uartRemoteHandle) != HAL_OK)
   {
      Error_Handler();
   }

   /* Activate interrupt on RX and TX */
   HAL_NVIC_SetPriority(USART6_IRQn, 2 , 0);
   HAL_NVIC_EnableIRQ(USART6_IRQn);
   __HAL_UART_DISABLE_IT(&uartRemoteHandle, UART_IT_RXNE);
   __HAL_UART_DISABLE_IT(&uartRemoteHandle, UART_IT_TXE);
   __HAL_UART_DISABLE_IT(&uartRemoteHandle, UART_IT_IDLE);

}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Initialize the UART peripheral to communicate with the remote control
  *
  * @param  None
  *
  * @note
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void uartBypassInit(void)
{
   /* Initialize the peripheral clock */
   __HAL_RCC_USART1_CLK_ENABLE();

   /* Configure the UART parameters */
   uartBypassHandle.Init.BaudRate = BYPASS_BAUDRATE;
   uartBypassHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
   uartBypassHandle.Init.Mode = UART_MODE_TX_RX;
   uartBypassHandle.Init.OverSampling = UART_OVERSAMPLING_16;
   uartBypassHandle.Init.Parity = UART_PARITY_NONE;
   uartBypassHandle.Init.StopBits = UART_STOPBITS_1;
   uartBypassHandle.Init.WordLength = UART_WORDLENGTH_8B;
   uartBypassHandle.Instance = USART1;
   if(HAL_UART_Init(&uartBypassHandle) != HAL_OK)
   {
      Error_Handler();
   }

   /* Activate interrupt on RX and TX */
   HAL_NVIC_SetPriority(USART1_IRQn, 2 , 0);
   HAL_NVIC_EnableIRQ(USART1_IRQn);
   __HAL_UART_DISABLE_IT(&uartBypassHandle, UART_IT_RXNE);
   __HAL_UART_DISABLE_IT(&uartBypassHandle, UART_IT_TXE);
   __HAL_UART_DISABLE_IT(&uartBypassHandle, UART_IT_IDLE);
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Copy the remote/bypass message to a local message
  *
  * @param  messageDestination: message will be copy in this buffer
  *         id: Select remote or bypass to be copy
  *
  * @note   Need to be call when uartBypassRecvFlag or uartRemoteRecvFlag = true
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void uartCopyMessage(dlV2Message_ts *messageDestination, uartId_te id)
{
   uint8_t i = 0;

   if(id == uartBypass)
   {
      messageDestination->sof = bypassRecvMessage.sof;
      messageDestination->src = bypassRecvMessage.src;
      messageDestination->dst = bypassRecvMessage.dst;
      messageDestination->code = bypassRecvMessage.code;
      messageDestination->count = bypassRecvMessage.count;
      for (i = 0; i < messageDestination->count; ++i)
      {
         messageDestination->data[i] = bypassRecvMessage.data[i];
      }
      messageDestination->cs = bypassRecvMessage.cs;
      messageDestination->eof = bypassRecvMessage.eof;
      messageDestination->status = bypassRecvMessage.status;
   }
   else if(id == uartRemote)
   {
      messageDestination->sof = remoteRecvMessage.sof;
      messageDestination->src = remoteRecvMessage.src;
      messageDestination->dst = remoteRecvMessage.dst;
      messageDestination->code = remoteRecvMessage.code;
      messageDestination->count = remoteRecvMessage.count;
      for (i = 0; i < messageDestination->count; ++i)
      {
         messageDestination->data[i] = remoteRecvMessage.data[i];
      }
      messageDestination->cs = remoteRecvMessage.cs;
      messageDestination->eof = remoteRecvMessage.eof;
      messageDestination->status = remoteRecvMessage.status;
   }
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Prepare the UART for the TX operation by taking a copy of the buffer
  *      and the amount of bytes to send.Then, it sends the first byte and set
  *      the TX status to Busy, so we don't overwrite the buffer to send if this
  *      function is called again.
  *
  * @param  device:  Specifies the UART device to work on.
  *      nbBytes:    Specifies the amount of bytes to transfer.
  *      dataBuffer: The data to send over UART. The data will be copie in a local buffer
  *
  * @note 
  *
  * @retval 0: Success or add to fifo
  *         1: Data fail to add to fifo
  *         2: Fail to send data, buffer to small
  *
  *-----------------------------------------------------------------------------
  */
uint8_t uartSendBytes (uartId_te device,  uint16_t nbBytes, uint8_t *dataBuffer)
{
   uint16_t byte_counter = 0;

   switch (device)
   {
      case uartRemote:
         if(remoteTx.uartTxStatus == uartTxReady)
         {
            /* Set the amount of byte to send*/
            remoteTx.uartTxBytesSent = 0;
            remoteTx.uartTxBytesToSend = nbBytes;

            if(nbBytes > DLV2_MAX_MESSAGE_SIZE)   // Verify the size of the buffer before copy
            {
              return 2;
            }
            /* Copy the buffer to send over UART */
            for(byte_counter = 0; byte_counter < nbBytes; byte_counter++)
            {
              remoteTx.uartTxBuffer[byte_counter] = dataBuffer[byte_counter];
            }

            /* The UART TX is now busy */
            remoteTx.uartTxStatus = uartTxBusy;

            /* Sends the first byte */
            USART6->DR = remoteTx.uartTxBuffer[remoteTx.uartTxBytesSent];

            remoteTx.uartTxBytesSent++;
            __HAL_UART_ENABLE_IT(&uartRemoteHandle, UART_IT_TXE);
         }
         else
         {
            if(fifoAddData(&(remoteTx.uartTxFifo), dataBuffer, nbBytes) != fifoOk)
            {
              return 1;
            }
         }
         break;

      case uartBypass:
         break;
   }

   return 0;
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Disable the Rx interrupt
  *
  * @param  id: Select remote or bypass to be copy
  *
  * @note   None
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void uartDisableRxInterrupt(uartId_te id)
{
  if(id == uartRemote)
  {
      __HAL_UART_DISABLE_IT(&uartRemoteHandle, UART_IT_RXNE);
  }
  else if(id == uartBypass)
  {
      __HAL_UART_DISABLE_IT(&uartBypassHandle, UART_IT_RXNE);
  }
}

/**
  *-----------------------------------------------------------------------------
  *
  * @brief  Enable the Rx interrupt
  *
  * @param  id: Select remote or bypass to be copy
  *
  * @note   None
  *
  * @retval None
  *
  *-----------------------------------------------------------------------------
  */
void uartEnableRxInterrupt(uartId_te id)
{
  if(id == uartRemote)
  {
      __HAL_UART_ENABLE_IT(&uartRemoteHandle, UART_IT_RXNE);
  }
  else if(id == uartBypass)
  {
      __HAL_UART_ENABLE_IT(&uartBypassHandle, UART_IT_RXNE);
  }
}
/* ---------------------------------------------------------------------------*/
