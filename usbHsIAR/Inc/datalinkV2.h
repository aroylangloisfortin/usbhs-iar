/**
  *-----------------------------------------------------------------------------
  * @file    datalinkV2.h
  * @author  Fortin Auto Radio, Inc.
  * @date    20-04-2017
  * @brief
  *-----------------------------------------------------------------------------
  */

#ifndef _DATALINKV2_H_
#define _DATALINKV2_H_

/* Includes ------------------------------------------------------------------*/

#ifdef _DATALINKV2_GLOBAL
    #define EXTERN
#else
    #define EXTERN extern
#endif


/* Global Defines ------------------------------------------------------------*/
#define DLV2_MAX_COUNT_SIZE 25
#define DLV2_MAX_MESSAGE_SIZE 32

#define DLV2_SOF              (uint8_t)0x0C
#define DLV2_EOF              (uint8_t)0x0D

// Node address
#define DLV2_NODE_ROUTER              (uint8_t)0x00
#define DLV2_NODE_WEBLINK_PC          (uint8_t)0x01
#define DLV2_NODE_PROGRAMMER          (uint8_t)0x02
#define DLV2_NODE_REMOTE_STARTER      (uint8_t)0x03
#define DLV2_NODE_ALARM               (uint8_t)0x04
#define DLV2_NODE_KEYLESS             (uint8_t)0x05
#define DLV2_NODE_TRANSPONDER_BYPASS  (uint8_t)0x06
#define DLV2_NODE_DOOR_LOCK_INT       (uint8_t)0x07
#define DLV2_NODE_CELL_COMM_DEVICE    (uint8_t)0x08
#define DLV2_NODE_EXT_INT             (uint8_t)0x09
#define DLV2_NODE_PAGER_INT           (uint8_t)0x0A
#define DLV2_NODE_ALL                 (uint8_t)0xFF

/* Global Typedef ------------------------------------------------------------*/
typedef enum
{
    unknow,
    dataReady,
    checksumError,
    eofError,
    csAndEofError
}dlV2RecvStatus_te;

typedef struct
{
    uint8_t sof;
    uint8_t src;
    uint8_t dst;
    uint8_t code;
    uint8_t count;
    uint8_t data[DLV2_MAX_COUNT_SIZE];
    uint8_t cs;
    uint8_t eof;
    dlV2RecvStatus_te status;
}dlV2Message_ts;

typedef enum
{
   dlV2Remote,
   dlV2Bypass,
   dlV2Pc
}dlV2Id_te;
/* Global Variables ----------------------------------------------------------*/

/* Global Functions Prototypes -----------------------------------------------*/
void     dlV2RecvFSM(dlV2Message_ts* recvMessage, uint8_t recvByte, dlV2Id_te id);
uint8_t  dlV2Checksum(dlV2Message_ts message);
uint8_t  dlV2Serializer(dlV2Message_ts* message, uint8_t* serialBuffer, uint8_t serialBufferSize);

/* ---------------------------------------------------------------------------*/

#undef EXTERN
#endif //_DATALINKV2_H_
