/**
  *-----------------------------------------------------------------------------
  * @file    uartDriver.h
  * @author  Fortin Auto Radio, Inc.
  * @date    20-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

#ifndef _UART_DRIVER_H_
#define _UART_DRIVER_H_

/* Includes ------------------------------------------------------------------*/
#include "datalinkV2.h"

#ifdef _UART_DRIVER_GLOBAL
    #define EXTERN 
#else
    #define EXTERN extern
#endif

/* Global Defines ------------------------------------------------------------*/

/* Global Typedef ------------------------------------------------------------*/
typedef enum
{
   uartRemote,
   uartBypass
}uartId_te;

/* Global Variables ----------------------------------------------------------*/
#ifdef _UART_DRIVER_GLOBAL
   EXTERN bool uartBypassRecvFlag = false;
   EXTERN bool uartRemoteRecvFlag = false;
#else
   EXTERN bool uartBypassRecvFlag;
   EXTERN bool uartRemoteRecvFlag;
#endif

/* Global Functions Prototypes -----------------------------------------------*/
void     uartRemoteInit(void);
void     uartBypassInit(void);
void     uartCopyMessage(dlV2Message_ts *messageDestination, uartId_te id);
uint8_t  uartSendBytes (uartId_te device,  uint16_t nbBytes, uint8_t *dataBuffer);
void uartEnableRxInterrupt(uartId_te id);
void uartDisableRxInterrupt(uartId_te id);

/* ---------------------------------------------------------------------------*/

#undef EXTERN 
#endif //_UART_DRIVER_H_
