/**
  *-----------------------------------------------------------------------------
  * @file    usbDriver.h
  * @author  Fortin Auto Radio, Inc.
  * @date    20-04-2017
  * @brief
  *-----------------------------------------------------------------------------
  */

#ifndef _USB_DRIVER_H_
#define _USB_DRIVER_H_

/* Includes ------------------------------------------------------------------*/
#include "usbd_def.h"
#include "global.h"

#ifdef _USB_DRIVER_GLOBAL
    #define EXTERN
#else
    #define EXTERN extern
#endif


/* Global Defines ------------------------------------------------------------*/
#define  FIFO_SIZE 4
#define  FIFO_WIDTH 256

/* Global Typedef ------------------------------------------------------------*/
typedef struct FIFO
{
   uint32_t fifoPos;
   uint8_t  fifoData[FIFO_SIZE][FIFO_WIDTH];
   uint32_t fifoDataPos[FIFO_SIZE];
}fifo_ts;

typedef enum
{
   dataReceive,
   noDataReady,
   bufferToSmall
}usbRecvStatus_te;

/* Global Variables ----------------------------------------------------------*/
#ifdef _USB_DRIVER_GLOBAL
EXTERN fifo_ts usbRxFifo = {.fifoPos = 0};
#else
EXTERN fifo_ts usbRxFifo;
#endif

EXTERN USBD_HandleTypeDef hUsbDeviceHS;

/* Global Functions Prototypes -----------------------------------------------*/
void     usbInit(void);
void     usbSend(uint8_t* buf, uint16_t len);
usbRecvStatus_te  usbReceive(uint8_t *buf, uint32_t bufLen, uint32_t *messageLen);
/* ---------------------------------------------------------------------------*/

#undef EXTERN
#endif //_USB_DRIVER_H_

