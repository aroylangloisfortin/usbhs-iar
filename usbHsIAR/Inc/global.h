/**
  *-----------------------------------------------------------------------------
  * @file    global.h
  * @author  Fortin Auto Radio, Inc.
  * @date    15-04-2017
  * @brief   Global definition to all files
  *-----------------------------------------------------------------------------
  */

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

/* Includes ------------------------------------------------------------------*/
#include "stdlib.h"
#include "stdio.h"
#include "stdint.h"
#include "stm32f2xx.h"
#include "stdbool.h"

/* Global Defines ------------------------------------------------------------*/
#define LOW (bool)0
#define HIGH (bool)1

/* Global Typedef ------------------------------------------------------------*/

/* Global Variables ----------------------------------------------------------*/
#define     NUM_OF_ELEMENTS(x)              sizeof(x)/sizeof(*x)

/* Global Functions Prototypes -----------------------------------------------*/
void Error_Handler(void);
void SystemClock_Config(void);

/* ---------------------------------------------------------------------------*/

#undef EXTERN
#endif //_GLOBAL_H_
