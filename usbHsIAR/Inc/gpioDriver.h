/**
  *-----------------------------------------------------------------------------
  * @file    gpioDriver.h
  * @author  Fortin Auto Radio, Inc.
  * @date    20-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

#ifndef _GPIO_DRIVER_H_
#define _GPIO_DRIVER_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal_gpio.h"

#ifdef _GPIO_DRIVER_GLOBAL
    #define EXTERN 
#else
    #define EXTERN extern
#endif


/* Global Defines ------------------------------------------------------------*/


/* Global Typedef ------------------------------------------------------------*/
typedef struct
{
    uint16_t pin;
    GPIO_TypeDef* port;
}gpio_ts;

/* Global Variables ----------------------------------------------------------*/
#ifdef _GPIO_DRIVER_GLOBAL
   EXTERN const gpio_ts gpioUartRemoteTx = {.pin = GPIO_PIN_6, .port = GPIOC};
   EXTERN const gpio_ts gpioUartRemoteRx = {.pin = GPIO_PIN_7, .port = GPIOC};
   EXTERN const gpio_ts gpioUartBypassTx = {.pin = GPIO_PIN_9, .port = GPIOA};
   EXTERN const gpio_ts gpioUartBypassRx = {.pin = GPIO_PIN_10, .port = GPIOA};

   EXTERN const gpio_ts gpioLed1 = {.pin = GPIO_PIN_0, .port = GPIOB};
   EXTERN const gpio_ts gpioLed2 = {.pin = GPIO_PIN_1, .port = GPIOB};
   EXTERN const gpio_ts gpioLed3 = {.pin = GPIO_PIN_2, .port = GPIOB};

#else
   EXTERN const gpio_ts gpioUartRemoteTx;
   EXTERN const gpio_ts gpioUartRemoteRx;
   EXTERN const gpio_ts gpioUartBypassTx;
   EXTERN const gpio_ts gpioUartBypassRx;

   EXTERN const gpio_ts gpioLed1;
   EXTERN const gpio_ts gpioLed2;
   EXTERN const gpio_ts gpioLed3;
#endif


/* Global Functions Prototypes -----------------------------------------------*/
void gpioInit(void);
void gpioUartRemoteInit(void);
void gpioUartBypassInit(void);
void gpioLedInit(void);
void gpioWritePin(gpio_ts gpio, bool pinState);

/* ---------------------------------------------------------------------------*/

#undef EXTERN 
#endif //_GPIO_DRIVER_H_
