/**
  *-----------------------------------------------------------------------------
  * @file    fifo.h
  * @author  Fortin Auto Radio, Inc.
  * @date    20-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

#ifndef _FIFO_H_
#define _FIFO_H_

/* Includes ------------------------------------------------------------------*/

#ifdef _FIFO_GLOBAL
    #define EXTERN 
#else
    #define EXTERN extern
#endif


/* Global Defines ------------------------------------------------------------*/
#define FIFO_SIZE 5
#define FIFO_WIDTH 40

/* Global Typedef ------------------------------------------------------------*/
typedef struct
{
	uint8_t tail;
	uint8_t head;
	uint8_t data[FIFO_SIZE][FIFO_WIDTH];
        uint8_t dataSize[FIFO_SIZE];
}fifo_ts;

typedef enum
{
	fifoOk,
	fifoWidthToSmall,
	fifoSizeToSmall,
	fifoNoData
}fifoStatus_te;

/* Global Variables ----------------------------------------------------------*/

/* Global Functions Prototypes -----------------------------------------------*/
fifoStatus_te fifoAddData(fifo_ts *fifo, uint8_t *data, uint8_t dataSize);
fifoStatus_te fifoRemoveData(fifo_ts *fifo, uint8_t *data, uint8_t dataSize, uint8_t *dataSizeCopied);

/* ---------------------------------------------------------------------------*/

#undef EXTERN 
#endif //_FIFO_H_