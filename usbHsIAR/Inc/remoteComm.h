/**
  *-----------------------------------------------------------------------------
  * @file    remoteComm.h
  * @author  Fortin Auto Radio, Inc.
  * @date    20-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

#ifndef _REMOTE_COMM_H_
#define _REMOTE_COMM_H_

/* Includes ------------------------------------------------------------------*/

#ifdef _REMOTE_COMM_GLOBAL
    #define EXTERN 
#else
    #define EXTERN extern
#endif


/* Global Defines ------------------------------------------------------------*/

/* Global Typedef ------------------------------------------------------------*/

/* Global Variables ----------------------------------------------------------*/

/* Global Functions Prototypes -----------------------------------------------*/
void remoteCommInit(void);
void remoteCommRun(void);
/* ---------------------------------------------------------------------------*/

#undef EXTERN 
#endif //_REMOTE_COMM_H_
