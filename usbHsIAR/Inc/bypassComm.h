/**
  *-----------------------------------------------------------------------------
  * @file    bypassComm.h
  * @author  Fortin Auto Radio, Inc.
  * @date    20-04-2017
  * @brief   
  *-----------------------------------------------------------------------------
  */

#ifndef _BYPASS_COMM_H_
#define _BYPASS_COMM_H_

/* Includes ------------------------------------------------------------------*/

#ifdef _BYPASS_COMM_GLOBAL
    #define EXTERN 
#else
    #define EXTERN extern
#endif


/* Global Defines ------------------------------------------------------------*/

/* Global Typedef ------------------------------------------------------------*/

/* Global Variables ----------------------------------------------------------*/

/* Global Functions Prototypes -----------------------------------------------*/
void bypassCommInit(void);
void bypassCommRun(void);
/* ---------------------------------------------------------------------------*/

#undef EXTERN 
#endif //_BYPASS_COMM_H_
